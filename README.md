Minetest Anti-Proxy Mod created by shivajiva

This mod uses getipintel.net to check a players IP against known proxy IP's 
kicking the player if they are a proxy.
It caches player IP addresses and prevents abuse of the service by capping 
the requests to <500 / 24 hours.

Useful for server administrators

