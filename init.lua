-- check player IP for proxy
-- by shivajiva101@hotmail for minetest voxel game

noproxy = {}
noproxy.cache = {}
noproxy.cache.start_time = 0
noproxy.cache.hits = 0

local ttl = 172800 -- 48 hours
local cap = 499
local email = "" -- set this before running the mod!
local http = minetest.request_http_api()

if not http then
  print("ERROR: in minetest.conf, this mod must be in secure.http_mods!")
end

------------------------------
-- Load/save Functions
------------------------------
-- load
local function load_data()
  local file = io.open(minetest.get_worldpath().."/dip", "r")
  if file then
    local table = minetest.deserialize(file:read("*all"))
    if type(table) == "table" then
      noproxy.cache = table
      return
    end
  end
end
load_data()

-- save
noproxy.save_data = function()
  local file = io.open(minetest.get_worldpath().."/dip", "w")
  if file then
    file:write(minetest.serialize(noproxy.cache))
    file:close()
  end
end

-- initialise start time on load
if noproxy.cache.start_time == 0 then
  noproxy.cache.start_time = os.time()
  noproxy.save_data()
end

-- intercept player joining
minetest.register_on_prejoinplayer(function(name, ip)

    local chk = false
    local diff = 0
    local now = os.time()

    -- Check if ip is cached
    if noproxy.cache[ip] then
      -- calc diff
      diff = now - noproxy.cache[ip].timestamp
      if diff > ttl then -- exceeded ttl?
        chk = true -- flag IP check
        noproxy.cache[ip].timestamp = now -- store time
      else -- use cache instead
        if noproxy.cache[ip].proxy then -- proxy?
          return ("\nConnection timed out")
        end
      end
    else
      -- max 500 hits in 24 hours
      if noproxy.cache.hits < cap then
        -- initialise
        noproxy.cache[ip] = {timestamp=now, proxy=false}
        chk = true
      else
        -- check if it can be reset
        diff = now - noproxy.cache.start_time
        -- 24 hours passed?
        if diff >= 86400 then
          -- reinitialise
          noproxy.cache.hits = 0
          noproxy.cache.start_time = now
          noproxy.cache[ip] = {timestamp=now, proxy=false}
          chk = true
        end
      end
    end

    if chk then -- check ip?
      chk = noproxy.check_ip(ip)
      -- no proxy
      if chk == "0" then 
        minetest.log("action", "HTTP request responded no proxy @ "..ip)
        noproxy.save_data()
        return
      end
      -- proxy
      if chk == "1" then --proxy?
        -- set proxy flag
        noproxy.cache[ip].proxy = true
        noproxy.save_data()
        return ("\nConnection timed out")
      end
      -- no data
      if chk == "" then -- failed request?
        minetest.log("action", "HTTP request failed: no data returned")
        return
      end
    end
  end
)

noproxy.check_ip = function(ip)

  local hdl = {} -- async request handle
  local res = {completed=false} -- partially initialise response

  -- request ip check
  local hdl = http.fetch_async({
      url = "http://check.getipintel.net/check.php?ip="..ip
      .."&contact="..email.."&flags=m",
      timeout = 1
    })
  -- loop until we get a completed reponse - blocking, must be a better way?
  while not res.completed do
    res = http.fetch_async_get(hdl)
  end
  -- increment cap tracker
  if not res.data == "" then
    noproxy.cache.hits = noproxy.cache.hits + 1
  end
  -- return response data
  return res.data
end
